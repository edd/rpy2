cffi>=1.15.1
jinja2
tzlocal

[:platform_system == "Windows"]
packaging

[:python_version < "3.8"]
typing-extensions

[:python_version < "3.9"]
backports.zoneinfo

[all]
pytest
ipython

[all:python_version < "3.10"]
pandas

[all:python_version < "3.9"]
numpy<1.26

[all:python_version >= "3.10"]
pandas>=1.3.5

[all:python_version >= "3.9"]
numpy>=1.26

[doc]
ipykernel
jupytext
multipledispatch
nbconvert
numpy
pandas
pygraphviz
sphinx

[numpy]

[numpy:python_version < "3.9"]
numpy<1.26

[numpy:python_version >= "3.9"]
numpy>=1.26

[pandas]

[pandas:python_version < "3.10"]
pandas

[pandas:python_version < "3.9"]
numpy<1.26

[pandas:python_version >= "3.10"]
pandas>=1.3.5

[pandas:python_version >= "3.9"]
numpy>=1.26

[test]
pytest
ipython

[test:python_version < "3.10"]
pandas

[test:python_version < "3.9"]
numpy<1.26

[test:python_version >= "3.10"]
pandas>=1.3.5

[test:python_version >= "3.9"]
numpy>=1.26

[test_minimal]
pytest>=8
coverage
pytest-cov

[types]
mypy
types-tzlocal
